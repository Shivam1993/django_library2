from django.contrib import admin
from books.models import Book,Category,Shelf,Binding

# Register your models here.
@admin.register(Binding)
class BindingAdmin(admin.ModelAdmin):
    list_display=('id','binding_name')
#admin.site.register(Binding,BindingAdmin)

@admin.register(Shelf)
class ShelfAdmin(admin.ModelAdmin):
    list_display=('id','shelf_no')
#admin.site.register(Shelf,ShelfAdmin)

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display=('id','category_name','shelf')
#admin.site.register(Category,CategoryAdmin)

@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display=('id','book_Code','publication_year','book_title','category','binding')
#admin.site.register(Book,BookAdmin)    

