from django.db import models
from user.models import Borrower

# Create your models here.
class Binding(models.Model):
    binding_name=models.CharField(max_length=70)
    def __str__(self):
        return self.binding_name

class Shelf(models.Model):
    shelf_no=models.CharField(max_length=50)
    def __str__(self):
        return self.shelf_no

class Category(models.Model):
    category_name=models.CharField(max_length=70)
    shelf=models.ForeignKey(Shelf,on_delete=models.CASCADE)
    def __str__(self):
        return self.category_name
    

class Book(models.Model):
    book_Code=models.IntegerField()
    publication_year=models.IntegerField()
    book_title=models.CharField(max_length=70)
    category=models.ForeignKey(Category,on_delete=models.CASCADE)
    binding=models.ForeignKey(Binding,on_delete=models.CASCADE)
    def __str__(self):
        return self.book_title


