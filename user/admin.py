from django.contrib import admin
from user.models import Borrower,Student,Staff

# Register your models here.
@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display=('id','name','gender','roll_no','department')
#admin.site.register(Student,StudentAdmin)

@admin.register(Staff)
class StaffAdmin(admin.ModelAdmin):
    list_display=('id','name','gender','department','designation')
#admin.site.register(Staff,StaffAdmin)

@admin.register(Borrower)
class BorrowerAdmin(admin.ModelAdmin):
    list_display=('id','student','staff')
#admin.site.register(Borrower,BorrowerAdmin)
