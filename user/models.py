
from django.db import models

# Create your models here.
class CommonInfo(models.Model):
    name=models.CharField(max_length=100,null=True)
    department=models.CharField(max_length=50,null=True)
    gender=models.CharField(max_length=10,null=True)
    class meta:
        abstract=True
        
class Student(CommonInfo):
    roll_no=models.IntegerField(null=True)
    def __str__(self):
        return self.name

class Staff(CommonInfo):
    designation=models.CharField(max_length=50,null=True)
    def __str__(self):
        return self.name

class Borrower(models.Model):
    student=models.ForeignKey(Student,on_delete=models.CASCADE,null=True)
    staff=models.ForeignKey(Staff,on_delete=models.CASCADE,null=True)
    
    

